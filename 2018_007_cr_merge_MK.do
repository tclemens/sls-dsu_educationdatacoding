/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_merge.do
Task:         Merge all constructed datasets (apart from educational attainment)                                                   
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-01-15                                                                         
==============================================================================*/

use "${pdata}/main.dta", clear
merge 1:1 slsno using "${pdata}/mother.dta"
gen mothinf=1 if _merge==1
drop _merge
merge 1:1 slsno using "${pdata}/father.dta"
gen fathinf=1 if _merge==1
drop _merge
gen parinf=1 if mothinf==1 & fathinf==1
merge 1:1 slsno using "${pdata}/grandpar.dta"
drop if _merge==2
drop _merge
merge 1:1 slsno using "${pdata}/schoolcen.dta"
drop if _merge==1
drop _merge
merge 1:1 slsno using "${pdata}/attend.dta"
drop if _merge==1
drop _merge
merge 1:1 slsno using "${pdata}/exclude.dta"
drop if _merge==2
drop _merge



gen datazoneS3=datazone2001codeS3
sort datazoneS3
replace datazoneS3=datazone2001codeS4 if datazoneS3==""
tab datazoneS3,m
rename datazoneS3 datazone2001 // This is actually datazone in 2007 for both cohorts- one is S3 and the other in S4
merge m:1 datazone2001 using "${odata}/simd2006.dta"
drop if _merge==2
drop _merge
drop datazone2001

gen datazoneS5=datazone2001codeS5 if cohort==0
sort datazoneS5
replace datazoneS5=datazone2001codeS5 if datazoneS5=="" // note this is cohort==1
tab datazoneS5,m
rename datazoneS5 datazone2001 // This is actually datazone in 2009 for both cohorts- one is S5 and the other in S6
merge m:1 datazone2001 using "${odata}/simd2009.dta"
drop if _merge==2
drop _merge
drop datazone2001



/*sort slsno
merge 1:1 slsno using "${pdata}/smr02.dta"
tab _merge
drop if _merge==2
drop _merge
*/

drop if cpres==0 | cpres==4 // drop SLS members who have not been present during the census 2001
drop if parinf==1 // drop SLS members who did not live with their parents during the census 2001

*------------------------------------------------------------------------------*

*log using "${odata}/cr_merge.log", replace


describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/merge.dta", replace


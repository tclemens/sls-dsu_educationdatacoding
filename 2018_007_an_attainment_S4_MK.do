/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/



/*==============================================================================
File name:    an_attainment_S4.do
Task:         Analysis of attainment in S4                                                     
Project:      School absenteeism and attainment
Author(s):    SD, ES, MK                                           
Last update:  2021-09-09                                                                          
==============================================================================*/
/*------------------------------------------------------------------------------

Content:

#1 Merging data with S4 attainment data
#2 Missigness summary and pattern
#3 Summary statistics 
#4 Modelling total ucas score in S4
*/

use "${pdata}/attainment_S4.dta", clear

merge 1:1 slsno using "${pdata}/recode.dta"	// Recode is absenteeism merged with SES
drop if _merge==2 							// Delete cases from using that do not have SQA data
drop _merge
sort slsno

**-------------------------------------------------------------------------------------
***Exploring missing data characteristics***
**-------------------------------------------------------------------------------------

rename sex female

* Missingness summary and missingness pattern
misstable summarize female mothage age pareth urbrur granny iepS4 ///
 chealth parill parhelp pareduc simd2001_ext fsmS4 tenure2 parclasscat2 ///
 cohort propabsenceS4 proptruancyS4 tempexclS4cat propsicktotalS4 ///
 propfamholtotalS4 propexcepdomcirctotalS4 propothertotalS4

misstable pattern female mothage age pareth urbrur granny iepS4 ///
 chealth parill parhelp pareduc simd2001_ext fsmS4 tenure2 parclasscat2 ///
 cohort propabsenceS4 proptruancyS4 tempexclS4cat propsicktotalS4 ///
 propfamholtotalS4 propexcepdomcirctotalS4 propothertotalS4

* missing values counter
egen missnr=rowmiss(numbsib famstruc female mothage age pareth urbrur granny iepS4 ///
 chealth parill parhelp pareduc simd2001_ext fsmS4 tenure2 parclasscat ///
 cohort propabsenceS4 proptruancyS4 tempexclS4cat propsicktotalS4 ///
 propfamholtotalS4 propexcepdomcirctotalS4 totalucas) 

 * Sample size 
gen ansample=0
replace ansample=1 if missnr==0
keep if ansample==1


log using "${texts}/attainment_S4.log", replace

*-------------------------------------------------------------------------------
* Summary statistics
*-------------------------------------------------------------------------------

* Univariate statistics

kdensity totalucas
hist totalucas

* Continuous variables
estpost tabstat age mothage propabsenceS4 proptruancyS4 propfamholtotalS4 propexcepdomcirctotalS4 ///
propsicktotalS4 totalucas, ///
listwise stat(mean sd n) col(stat)

* Categorical variables
tab1 tempexclS4cat female pareth pareduc ///
simd2001_ext fsmS4 tenure2 parclasscat ///
urbrur cohort iepS4 granny chealth parill parhelp numbsib famstruc
 

** Bivariate statistics

*Correlations between exposure and outcome variables 
corr propabsenceS4 proptruancyS4 propsicktotalS4 numsubj totalucas

estpost tabstat totalucas, by(tempexclS4cat) s(mean sd n)

***	Are differences between descriptive statistics statistically significant?
ttest totalucas, by(tempexclS4cat)
ttest numsubj, by(tempexclS4cat)


* Create percentages from proportions for absenteeism measures

foreach var of varlist propabsenceS4 proptruancyS4  propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4  ///
{
replace `var'=`var'*100
}

* Standardise outcome variable
egen ztotalucas = std(totalucas)


**-------------------------------------------------------------------------------------
* Modelling achievement in S4 
**-------------------------------------------------------------------------------------


** Overall absenteeism
encode seedcodeS4, gen(school)

* Covariate model
reg ztotalucas propabsenceS4 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 ///
i.fsmS4 i.numbsib i.famstruc tempexclS4cat, ///
vce(cluster school) 


* School fixed effects model
xtset school
xtreg ztotalucas propabsenceS4 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 ///
i.fsmS4 i.numbsib i.famstruc tempexclS4cat, fe ///
vce(cluster school)
estimates store absenceS4


** Specific forms of absenteeism

* Covariate model
reg ztotalucas proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 ///
i.fsmS4 i.numbsib i.famstruc tempexclS4cat, vce(cluster school)

* School fixed effects model
xtreg ztotalucas proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 ///
i.fsmS4 i.numbsib i.famstruc tempexclS4cat, fe vce(cluster school)
estimate store absenceformS4

* Test of Differential relationships
test proptruancyS4=propsicktotalS4
test proptruancyS4=propfamholtotalS4
test proptruancyS4=propexcepdomcirctotalS4
test propsicktotalS4=propfamholtotalS4
test propsicktotalS4=propexcepdomcirctotalS4
test propfamholtotalS4=propexcepdomcirctotalS4




log close



		 
		  






/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_grandpar.do
Task:         Select Census variables for SLS members' parents                                                     
Project:      SES and school absenteeism
Author(s):    SD, ES, MK                                           
Last update:  2019-01-29                                                                           
==============================================================================*/

/*------------------------------------------------------------------------------
Notes:
  Variable for number of grandparent in house has only *** observations-
  That is for those whose grandparent was at home.
------------------------------------------------------------------------------*/

use "${odata}/grandparents.dta", clear

*------------------------------------------------------------------------------*
sort slsno
duplicates report slsno
gen grandpres=count_grandparent


*------------------------------------------------------------------------------*

*log using "${odata}/cr_grandpar.log", replace

keep slsno grandpres

describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/grandpar.dta", replace


*==============================================================================*

# SLS-DSU_EducationDataCoding

## Introduction

Syntax files created for the SLS Project 2018_007: "Social Inequalities in Educational Attainment: An Investigation into the Mediating Role of School Absenteeism". 

More project details can be found [here](https://sls.lscs.ac.uk/projects/view/2018_007/)

All syntax file created by the Project Team.

Please send all correspondence to the PI: Dr Markus Klein (University of Strathclyde) markus.klein@strath.ac.uk

<b>More details to follow</b>



/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_recode.do
Task:         Recoding of variables                                                    
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-10-20                                                                          
==============================================================================*/

use "${pdata}/merge.dta", clear
sort slsno


* Age
tab mbday
tostring mbday, generate(mbday2)
destring mbday2, generate(mbday3) // January 1960 is the reference point
gen age=(571-mbday3)/12 // Calculate age in years by substracting months at birth (reference point)  from total months at the start of S3 (***, also from reference point)
replace age=age-1 if cohort==1 // Generate age at S3 for cohort who is in S4 in 2007
tab age

* Sex
tab sex
replace sex=sex-1
lab def sex1 0 "Male" 1 "Female"
lab val sex sex1

* Number of siblings (at the time of the Census=2001)
tab numbsib, m
tab numbsib

* Family structure (at the time of Census=2001)
recode famstruc 4 7=1 5 8=2 1 2 9=3
lab def famstruc 1 "Couple Family" 2 "Couple Step-Family" 3 "Single parent"
lab val famstruc famstruc
tab famstruc, m

* Perceived health of the child (at the time of Census=2001)

recode chealth 2 3=2
lab def chealth 1 "Good" 2 "Not Good"
lab val chealth chealth
tab chealth, m 

* Housing tenure (at the time of Census=2001) 

recode tenure 0/2=1 5/8=2 3 4 9=3
lab def tenure 1 "Owned" 2 "Private rented" 3 "Social rented"
lab val tenure tenure
tab tenure, m

gen tenure2=tenure 
recode tenure2 1 2=0 3=1
lab def tenure2 0 "Owned and Private rented" 1 "Social rented"
lab val tenure2 tenure2
tab tenure2, m


* Urban-Rural residence classification (at the time of Census=2001)

gen urbrur=oa2001_urshs80_0
recode urbrur 1/3=0 4/8=1
lab def urbrur 0 "Urban" 1 "Rural"
lab val urbrur urbrur
tab urbrur, m 

* SIMD (at the time of Census=2001)

* SIMD Quintiles generated with external threshholds

gen simd2001_ext=dz2001_simdscore4_0
replace simd2001_ext=1 if simd2001_ext<=7.63
replace simd2001_ext=2 if simd2001_ext>7.63 & simd2001_ext<=13.49
replace simd2001_ext=3 if simd2001_ext>13.49 & simd2001_ext<=21.18
replace simd2001_ext=4 if simd2001_ext>21.18 & simd2001_ext<=33.9
replace simd2001_ext=5 if simd2001_ext>33.9 & simd2001_ext<=87.6
lab def simd2001_ext ///
1 "SIMD 5 (least deprived)" ///
2 "SIMD 4" ///
3 "SIMD 3" ///
4 "SIMD 2" ///
5 "SIMD 1 (most deprived)"
lab val simd2001_ext simd2001_ext

* SIMD quintiles generated from the data

tab dz2001_simdscore4_0
xtile simd2001_5=dz2001_simdscore4_0, n(5) // SIMD at 2001- before pupils started schooling

tab dz2001_eduscore4_0 
tab dz2001_empscore4_0
tab dz2001_healscore4_0 
tab dz2001_housscore4_0 
tab dz2001_incscore4_0 

**SIMD (in 2006 and 2009 based on datazone for 2007 and 2009 respectively)
xtile simd2006_5=simd2006score, n(5) // SIMD for S3 and S4
xtile simd2009_5=simd2009v2score, n(5) // SIMD for S5 and S6

corr simd2001_5 simd2006_5 simd2009_5 // mobility

corr simd2001_ext simd2001_5


* Educational deprivation at the household level
gen depeduc=depedh0
tab depeduc, m
label define depeduc 0 "No" 1 "Yes"
lab val depeduc depeduc

* Employment deprivation at the household level 
gen depemp=depemh0
tab depemp, m
lab define depemp 0 "No" 1 "Yes"
lab val depemp depemp

* Health deprivation at the household level
gen dephealth=dephdh0
tab dephealth, m
lab define dephealth 0 "No" 1 "Yes"
lab val dephealth dephealth

* Housing deprivation at the household level
gen dephouse=dephsh0
tab dephouse, m
lab def dephouse 0 "No" 1 "Yes"
lab val dephouse dephouse

* Tenure deprivation at the household level
gen deptenure=deptnh0
tab deptenure, m
lab def deptenure 0 "No" 1 "Yes"
lab val deptenure deptenure

* Parental education based on highest education
gen pareduc=motheduc
replace pareduc=fatheduc if fatheduc>motheduc & fatheduc!=.
replace pareduc=fatheduc if motheduc==.

list fatheduc motheduc pareduc if fatheduc==.
list fatheduc motheduc pareduc if motheduc==.
list fatheduc motheduc if pareduc==.
tab pareduc

recode pareduc 20=5 21=4 22=3 23=2 24=1

lab def pareduc 1 "First degree/higher degree/Professional qualifications" ///
2 "HNC/HND/SVQ level 4 or 5 etc" ///
3 "Higher grade/CSYS/‘A’ level, etc/GSVQ/SVQ Level 3/ONC/OND etc" ///
4 "‘O’ Grade/Standard grade/GCSE/CSE" ///
5 "No Qualifications"   
lab val pareduc pareduc
tab pareduc, m

* Parental education based on combination of both parents

gen fatheduc2=fatheduc
replace fatheduc2=fatheduc-19

gen motheduc2=motheduc
replace motheduc2=motheduc-19


gen pareduc_comb=.
replace pareduc_comb=9 if fatheduc2==5 & motheduc2==5
replace pareduc_comb=8 if fatheduc2==5 & motheduc2!=5
replace pareduc_comb=8 if fatheduc2!=5 & motheduc2==5


replace pareduc_comb=7 if fatheduc2==4 & motheduc2==4
replace pareduc_comb=6 if fatheduc2==4 & (motheduc2<4 | motheduc2==.)
replace pareduc_comb=6 if motheduc2==4 & (fatheduc2<4 | fatheduc2==.)

replace pareduc_comb=5 if fatheduc2==3 & motheduc2==3
replace pareduc_comb=4 if fatheduc2==3 & (motheduc2<3 | motheduc2==.)
replace pareduc_comb=4 if motheduc2==3 & (fatheduc2<3 | fatheduc2==.)


replace pareduc_comb=3 if fatheduc2==2 & motheduc2==2
replace pareduc_comb=2 if fatheduc2==2 & (motheduc2<2 | motheduc2==.)
replace pareduc_comb=2 if motheduc2==2 & (fatheduc2<2 | fatheduc2==.)

replace pareduc_comb=1 if fatheduc2==1 & motheduc2==1
replace pareduc_comb=1 if fatheduc2==1 & motheduc2==.
replace pareduc_comb=1 if fatheduc2==. & motheduc2==1

lab def pareduc_comb 1 "No qualifications" 2 "at least one parent with ‘O’ Grade" ///
3 "both parents with ‘O’ Grade" 4 "at least one parent with Higher grade" ///
5 "both parents with Higher grade" 6 "at least one parent with HNC" ///
7 "both parents with HNC" 8 "at least one parent with degree" ///
9 "both parents with degree"
lab val pareduc_comb pareduc_comb
tab pareduc_comb, m


* Parental illness

gen parill=mothill
replace parill=fathill if fathill==1 & (mothill==. | mothill==2)
replace parill=fathill if mothill==. & fathill!=1
tab parill,m
label define parill 1 "Yes" 2 "No"
label value parill parill

* Parental ethnicity

gen pareth=mothethnic
replace pareth=fathethnic if fathethnic==1 & (mothethnic==. | mothethnic==0)
replace pareth=fathethnic if mothethnic==. & fathethnic!=1
tab pareth, m
label define pareth 0 " Either White" 1 "None White"
label value pareth pareth


* Parental class based on highest class 

gen parclass=mothclass
replace parclass=fathclass if fathclass<mothclass & fathclass!=.
replace parclass=fathclass if mothclass==.
tab parclass, m

gen parclasscat=parclass
recode parclasscat 1/6=1 7/12=2 13/16=3 17/20=4 21/23=5 24/30=6 31/35=7 36/38=8 40=.
tab parclasscat, m

label define parclasscat 1 "Higher man, admin & prof occ" 2 "Lower man, admin & prof occ" 3 "Intermediate occ." ///
4 "Small employers & own account workers" 5 "Lower supervisory & tech occ." 6 "Semi-routine occ." ///
7 "Routine occ." 8 "Never worked & long-term unemployed"
label value parclasscat parclasscat
tab parclasscat


gen parclasscat2=parclasscat
recode parclasscat2 1/2=1 3=2 4=3 5=4 6/8=5


label define parclasscat2 1 "Higher managerial, administrative & professional occ." 2 "Intermediate occupations" 3 "Small employers & own account workers" /// 
4 "Lower supervisory & technical occupations" 5 "Semi-routine and routine occupations"
label value parclasscat2 parclasscat2
tab parclasscat2

list fathclass mothclass parclass if fathclass==.
list fathclass mothclass parclass if mothclass==.
list fathclass mothclass if parclass==.


* Parental caring status
gen parhelp=.
replace parhelp=0 if (mothhelp==1 & fathhelp==1) | (mothhelp==1 & fathhelp==.) | (mothhelp==. & fathhelp==1)
replace parhelp=1 if (mothhelp>1 & mothhelp!=.) | (fathhelp>1 & fathhelp!=.)
lab def parhelp 0 "No carer" 1 "Carer"
lab val parhelp parhelp
tab parhelp, m


*Mother's age
gen mothage=dobyr-mothyob
tab mothage, m
 

*Grandparent presence
tab grandpres, m
gen granny=grandpres
replace granny=0 if granny==.
replace granny=1 if granny!=0
tab granny

label define granny 1 "At least one grandparent" 0 "No grandparent"
label value granny granny


*Attendance and Absenteeism
*Overall absenteeism

tab attendanceS3, m
tab attendanceS3
tab possattS3
gen freqabsenceS3=possattS3-attendanceS3
tab freqabsenceS3
tab freqabsenceS3, m
tab freqabsenceS3 if cohort==0, m
summ freqabsenceS3, d
gen propabsenceS3=(freqabsenceS3/possattS3)
tab propabsenceS3



tab attendanceS4, m
replace attendanceS4=. if attendanceS4==0 // For those who never showed up in S4, i.e attendance=0
// treat these people as missing since they NEVER showed up in the school
bysort cohort: tab attendanceS4
bysort cohort: tab attendanceS4, m 
tab attendanceS4
tab possattS4
bysort cohort: summ possattS4
gen freqabsenceS4=possattS4-attendanceS4
tab freqabsenceS4
bysort cohort: tab freqabsenceS4
tab freqabsenceS4, m
bysort cohort: summ freqabsenceS4, d
gen propabsenceS4=(freqabsenceS4/possattS4)
tab propabsenceS4
bysort cohort: tab propabsenceS4
bysort cohort: summ propabsenceS4
ttest propabsenceS4, by (cohort)


foreach var of varlist attendanceS5 excepdomcircauthS5 excepdomcircunauthS5 ///
famholauthS5 famholunauthS5 late1S5 late2S5 otherauthS5 otherunauthS5 ///
possattS5 sicknoedprovS5 sickwithedprovS5 tempexclS5 truancyS5 {
replace `var'=. if continueS5==0
}

tab attendanceS5, m
tab attendanceS5 if continueS5==1, m // The difference between missing and missing on continuation is DROPOUT
tab continueS5, m
replace attendanceS5=. if attendanceS5==0 // For those who never showed up in S5, i.e attendance=0
// treat these people as missing since they NEVER showed up in the school
bysort cohort: tab attendanceS5
bysort cohort: tab attendanceS5, m 
tab attendanceS5
tab possattS5
bysort cohort: summ possattS5
gen freqabsenceS5=possattS5-attendanceS5
tab freqabsenceS5
bysort cohort: tab freqabsenceS5
tab freqabsenceS5, m
summ freqabsenceS5, d
bysort cohort: summ freqabsenceS5, d
gen propabsenceS5=(freqabsenceS5/possattS5)
tab propabsenceS5
bysort cohort: tab propabsenceS5
bysort cohort: summ propabsenceS5
ttest propabsenceS5, by (cohort)


 

foreach var of varlist attendanceS6 excepdomcircauthS6 excepdomcircunauthS6 ///
famholauthS6 famholunauthS6 late1S6 late2S6 otherauthS6 otherunauthS6 ///
possattS6 sicknoedprovS6 sickwithedprovS6 tempexclS6 truancyS6 {
replace `var'=. if continueS6==0
}

tab attendanceS6, m
tab attendanceS6 if continueS6==1, m // The difference between missing and missing on continuation is DROPOUT
tab continueS6, m
replace attendanceS6=. if attendanceS6==0 // For those who never showed up in S6, i.e attendance=0
// treat these people as missing since they NEVER showed up in the school



bysort cohort: tab attendanceS6
bysort cohort: tab attendanceS6, m 
tab attendanceS6
tab possattS6
bysort cohort: summ possattS6
bysort cohort: summ attendanceS6
gen freqabsenceS6=possattS6-attendanceS6
tab freqabsenceS6
bysort cohort: summ freqabsenceS6
tab freqabsenceS6, m
summ freqabsenceS6, d
bysort cohort: summ freqabsenceS6, d
gen propabsenceS6=(freqabsenceS6/possattS6)
tab propabsenceS6
bysort cohort: tab propabsenceS6
bysort cohort: summ propabsenceS6
ttest propabsenceS6, by (cohort)


**Truancy

tab truancyS3 if cohort==0, m
tab truancyS3
tab possattS3
gen proptruancyS3=(truancyS3/possattS3)
tab proptruancyS3

tab truancyS4, m
bysort cohort: tab truancyS4
bysort cohort: tab truancyS4, m 
tab truancyS4
bysort cohort: summ truancyS4
tab truancyS4, m
bysort cohort: summ truancyS4, d
gen proptruancyS4=(truancyS4/possattS4)
tab proptruancyS4
bysort cohort: tab proptruancyS4
bysort cohort: summ proptruancyS4
ttest proptruancyS4, by (cohort)

tab truancyS5, m
bysort cohort: tab truancyS5
bysort cohort: tab truancyS5 if continueS5==1, m 
tab truancyS5 if continueS5==1
bysort cohort: summ truancyS5
tab truancyS5, m
bysort cohort: summ truancyS5, d
gen proptruancyS5=(truancyS5/possattS5)
tab proptruancyS5
bysort cohort: tab proptruancyS5
bysort cohort: summ proptruancyS5
ttest proptruancyS5, by (cohort)

tab truancyS6, m
bysort cohort: tab truancyS6
bysort cohort: tab truancyS6 if continueS6==1, m 
tab truancyS6 if continueS6==1
bysort cohort: summ truancyS6
tab truancyS6, m
bysort cohort: summ truancyS6, d
gen proptruancyS6=(truancyS6/possattS6)
tab proptruancyS6
bysort cohort: tab proptruancyS6
bysort cohort: summ proptruancyS6
ttest proptruancyS6, by (cohort)


***Sickness related absence
tab sicknoedprovS3 if cohort==0, m
tab sickwithedprovS3 if cohort==0, m
tab sicknoedprovS3 if sickwithedprovS3>0 & sickwithedprovS3!=.
gen sicktotalS3=sicknoedprovS3
tab sicktotalS3 if cohort==0, m // sickness variable regardless of whether education was provided or not
// this does not make any difference because *** had education provided when sick
tab sicktotalS3 if sicktotalS3>possattS3
gen propsicktotalS3=(sicktotalS3/possattS3)
tab propsicktotalS3


tab sicknoedprovS4, m
tab sicknoedprovS4 if sickwithedprovS4>0 & sickwithedprovS4!=.
gen sicktotalS4=sicknoedprovS4
tab sicktotalS4, m
tab sicktotalS4 if sicktotalS4>possattS4
gen propsicktotalS4=(sicktotalS4/possattS4)
tab propsicktotalS4
bysort cohort: tab propsicktotalS4, m
bysort cohort: summ propsicktotalS4
ttest propsicktotalS4, by (cohort)

tab sicknoedprovS5, m
tab sickwithedprovS5, m
tab sicknoedprovS5 if sickwithedprovS5>0 & sickwithedprovS5!=.
gen sicktotalS5=sicknoedprovS5
tab sicktotalS5, m
tab sicktotalS5 if sicktotalS5>possattS5
gen propsicktotalS5=(sicktotalS5/possattS5)
tab propsicktotalS5
bysort cohort: tab propsicktotalS5, m
bysort cohort: summ propsicktotalS5
ttest propsicktotalS5, by (cohort)

tab sicknoedprovS6, m
tab sickwithedprovS6, m
tab sicknoedprovS6 if sickwithedprovS6>0 & sickwithedprovS6!=.
gen sicktotalS6=sicknoedprovS6
tab sicktotalS6, m
tab sicktotalS6 if sicktotalS6>possattS6
gen propsicktotalS6=(sicktotalS6/possattS6)
tab propsicktotalS6
bysort cohort: tab propsicktotalS6, m
bysort cohort: summ propsicktotalS6
ttest propsicktotalS6, by (cohort)



***Authorised absences

**Family holidays authorised
tab famholauthS3 if cohort==0, m
gen propfamholauthS3=(famholauthS3/possattS3)
tab propfamholauthS3 if cohort==0, m

tab famholauthS4, m
gen propfamholauthS4=(famholauthS4/possattS4)
tab propfamholauthS4, m

tab famholauthS5, m
gen propfamholauthS5=(famholauthS5/possattS5)
tab propfamholauthS5, m

tab famholauthS6, m
gen propfamholauthS6=(famholauthS6/possattS6)
tab propfamholauthS6, m


**Exceptional circumstances authorised

tab excepdomcircauthS3 if cohort==0, m
gen propexcepdomcircauthS3prop=(excepdomcircauthS3/possattS3)
tab propexcepdomcircauthS3 if cohort==0, m

tab excepdomcircauthS4, m
gen propexcepdomcircauthS4=(excepdomcircauthS4/possattS4)
tab propexcepdomcircauthS4, m

tab excepdomcircauthS5, m
gen propexcepdomcircauthS5=(excepdomcircauthS5/possattS5)
tab propexcepdomcircauthS5, m

tab excepdomcircauthS6, m
gen propexcepdomcircauthS6=(excepdomcircauthS6/possattS6)
tab propexcepdomcircauthS6, m

**Other authorised
tab otherauthS3 if cohort==0, m
gen propotherauthS3=(otherauthS3/possattS3)
tab propotherauthS3 if cohort==0, m

tab otherauthS4, m
gen propotherauthS4=(otherauthS4/possattS4)
tab propotherauthS4, m

tab otherauthS5, m
gen propotherauthS5=(otherauthS5/possattS5)
tab propotherauthS5, m

tab otherauthS6, m
gen propotherauthS6=(otherauthS6/possattS6)
tab propotherauthS6, m


***Unauthorised absences

*Family holidays unauthorised
tab famholunauthS3 if cohort==0, m
gen propfamholunauthS3=(famholunauthS3/possattS3)
tab propfamholunauthS3 if cohort==0, m


tab famholunauthS4, m
gen propfamholunauthS4=(famholunauthS4/possattS4)
tab propfamholunauthS4, m


tab famholunauthS5, m
gen propfamholunauthS5=(famholunauthS5/possattS5)
tab propfamholunauthS5, m


tab famholunauthS6, m
gen propfamholunauthS6=(famholunauthS6/possattS6)
tab propfamholunauthS6, m


**Exceptional circumstances unauthorised
tab excepdomcircunauthS3 if cohort==0, m
gen propexcepdomcircunauthS3=(excepdomcircunauthS3/possattS3)
tab propexcepdomcircunauthS3 if cohort==0, m

tab excepdomcircunauthS4, m
gen propexcepdomcircunauthS4=(excepdomcircunauthS4/possattS4)
tab propexcepdomcircunauthS4, m

tab excepdomcircunauthS5, m
gen propexcepdomcircunauthS5=(excepdomcircunauthS5/possattS5)
tab propexcepdomcircunauthS5, m

tab excepdomcircunauthS6, m
gen propexcepdomcircunauthS6=(excepdomcircunauthS6/possattS6)
tab propexcepdomcircunauthS6, m

**Other unauthorised absences
tab otherunauthS3 if cohort==0, m
gen propotherunauthS3=(otherunauthS3/possattS3)
tab propotherunauthS3 if cohort==0, m

tab otherunauthS4, m
gen propotherunauthS4=(otherunauthS4/possattS4)
tab propotherunauthS4, m

tab otherunauthS5, m
gen propotherunauthS5=(otherunauthS5/possattS5)
tab propotherunauthS5, m

tab otherunauthS6, m
gen propotherunauthS6=(otherunauthS6/possattS6)
tab propotherunauthS6, m

** Overall family holidays
tab famholauthS3 if cohort==0, m
tab famholunauthS3 if cohort==0, m
gen famholtotalS3=famholauthS3+famholunauthS3 if cohort==0
tab famholtotalS3 if cohort==0, m
gen propfamholtotalS3=(famholtotalS3/possattS3)

tab famholauthS4, m
tab famholunauthS4, m
gen famholtotalS4=famholauthS4+famholunauthS4
tab famholtotalS4, m
gen propfamholtotalS4=(famholtotalS4/possattS4)


tab famholauthS5, m
tab famholunauthS5, m
gen famholtotalS5=famholauthS5+famholunauthS5
tab famholtotalS5, m
gen propfamholtotalS5=(famholtotalS5/possattS5)


tab famholauthS6, m
tab famholunauthS6, m
gen famholtotalS6=famholauthS6+famholunauthS6
tab famholtotalS6, m
gen propfamholtotalS6=(famholtotalS6/possattS6)

** Overall exceptional circumstances
tab excepdomcircauthS3 if cohort==0, m
tab excepdomcircunauthS3 if cohort==0, m
gen excepdomcirctotalS3=excepdomcircauthS3+excepdomcircunauthS3 if cohort==0
tab excepdomcirctotalS3 if cohort==0, m
gen propexcepdomcirctotalS3=(excepdomcirctotalS3/possattS3)

tab excepdomcircauthS4, m
tab excepdomcircunauthS4, m
gen excepdomcirctotalS4=excepdomcircauthS4+excepdomcircunauthS4
tab excepdomcirctotalS4, m
gen propexcepdomcirctotalS4=(excepdomcirctotalS4/possattS4)


tab excepdomcircauthS5, m
tab excepdomcircunauthS5, m
gen excepdomcirctotalS5=excepdomcircauthS5+excepdomcircunauthS5
tab excepdomcirctotalS5, m
gen propexcepdomcirctotalS5=(excepdomcirctotalS5/possattS5)


tab excepdomcircauthS6, m
tab excepdomcircunauthS6, m
gen excepdomcirctotalS6=excepdomcircauthS6+excepdomcircunauthS6
tab excepdomcirctotalS6, m
gen propexcepdomcirctotalS6=(excepdomcirctotalS6/possattS6)


** Overall family holidays
tab otherauthS3 if cohort==0, m
tab otherunauthS3 if cohort==0, m
gen othertotalS3=otherauthS3+otherunauthS3 if cohort==0
tab othertotalS3 if cohort==0, m
gen propothertotalS3=(othertotalS3/possattS3)

tab otherauthS4, m
tab otherunauthS4, m
gen othertotalS4=otherauthS4+otherunauthS4
tab othertotalS4, m
gen propothertotalS4=(othertotalS4/possattS4)


tab otherauthS5, m
tab otherunauthS5, m
gen othertotalS5=otherauthS5+otherunauthS5
tab othertotalS5, m
gen propothertotalS5=(othertotalS5/possattS5)


tab otherauthS6, m
tab otherunauthS6, m
gen othertotalS6=otherauthS6+otherunauthS6
tab othertotalS6, m
gen propothertotalS6=(othertotalS6/possattS6)





***Lateness
**Given small numbers of students who are never late, keep late1 to a continuous variable 
// then late2 as a binary variable- also because of the differences in definitions 

tab late1S3 if cohort==0, m
tab late2S3 if cohort==0, m

tab late1S3
tab late1S3 if late1S3>possattS3
gen proplate1S3=(late1S3/possattS3)
tab proplate1S3 if cohort==0, m

tab late2S3
tab late2S3 if late2S3>possattS3
gen proplate2S3=(late2S3/possattS3)
tab proplate2S3
gen verylateS3=proplate2S3
recode verylateS3 0=0 else=1 if cohort==0 & verylateS3!=.
tab verylateS3 if cohort==0, m

tab late1S4, m 
tab late2S4, m

tab late1S4
tab late1S4 if late1S4>possattS4
gen proplate1S4=(late1S4/possattS4)
tab proplate1S4, m

tab late2S4
tab late2S4 if late2S4>possattS4
gen proplate2S4=(late2S4/possattS4)
tab proplate2S4
gen verylateS4=proplate2S4
recode verylateS4 0=0 else=1 if verylateS4!=.
tab verylateS4, m

tab late1S5, m 
tab late2S5, m

tab late1S5
tab late1S5 if late1S5>possattS5
gen proplate1S5=(late1S5/possattS5)
tab proplate1S5, m

tab late2S5
tab late2S5 if late2S5>possattS5
gen proplate2S5=(late2S5/possattS5)
tab proplate2S5
gen verylateS5=proplate2S5
recode verylateS5 0=0 else=1 if verylateS5!=.
tab verylateS5, m

tab late1S6, m 
tab late2S6, m

tab late1S6
tab late1S6 if late1S6>possattS6
gen proplate1S6=(late1S6/possattS6)
tab proplate1S6, m

tab late2S6
tab late2S6 if late2S6>possattS6
gen proplate2S6=(late2S6/possattS6)
tab proplate2S6
gen verylateS6=proplate2S6
recode verylateS6 0=0 else=1 if verylateS6!=.
tab verylateS6, m



*Temporal exclusion
**Treat temporal exclusion as binary variable using the tempexcl variable in the attendance data 
// due to inconsistency with the data from exclusion dataset.

tab tempexclS3
tab tempexclS3, m
gen tempexclS3cat=0 if tempexclS3==0 & cohort==0
replace tempexclS3cat=1 if tempexclS3>0 & tempexclS3!=. & cohort==0
tab tempexclS3cat
lab def tempexclS3cat 0 "Never excluded" 1 "Excluded"
lab val tempexclS3cat tempexclS3cat
tab tempexclS3cat if cohort==0, m


bysort cohort: tab tempexclS4
tab tempexclS4, m
gen tempexclS4cat=0 if tempexclS4==0
replace tempexclS4cat=1 if tempexclS4>0 & tempexclS4!=.
tab tempexclS4cat
lab def tempexclS4cat 0 "Never excluded" 1 "Excluded"
lab val tempexclS4cat tempexclS4cat
bysort cohort: tab tempexclS4cat, m


**Compare exclusion from exlusion dataset to exclusion in attendance dataset
tab totdurationS4,m
gen test=totdurationS4
recode test .=0 else=1 if tempexclS4!=.
replace test=. if test>1
tab tempexclS4cat test
replace totexclnoprovS4=. if tempexclS4==.
replace totdurationS4=. if tempexclS4==.
gen diff=totdurationS4-totexclnoprovS4 // A majority of pupils have days of no provision that exceeds their total days of exclusion



bysort cohort: tab tempexclS5
tab tempexclS5, m
gen tempexclS5cat=0 if tempexclS5==0
replace tempexclS5cat=1 if tempexclS5>0 & tempexclS5!=.
tab tempexclS5cat
lab def tempexclS5cat 0 "Never excluded" 1 "Excluded"
lab val tempexclS5cat tempexclS5cat
bysort cohort: tab tempexclS5cat, m

bysort cohort: tab tempexclS6
tab tempexclS6, m
gen tempexclS6cat=0 if tempexclS6==0
replace tempexclS6cat=1 if tempexclS6>0 & tempexclS6!=.
tab tempexclS6cat
lab def tempexclS6cat 0 "Never excluded" 1 "Excluded"
lab val tempexclS6cat tempexclS6cat
bysort cohort: tab tempexclS6cat, m


*------------------------------------------------------------------------------*

*log using "${odata}/cr_recode.log", replace
keep slsno seedcodeS4 lacodeS4 sex age mothage pareth parclass pareduc simd2001_ext ///
fsmS4 tenure2 pareduc_comb numbsib famstruc parclass parclasscat parclasscat2 urbrur cohort ///
granny chealth parill parhelp ///
propabsenceS4 proptruancyS4 tempexclS4cat propsicktotalS4 ///
propfamholtotalS4 propexcepdomcirctotalS4 propothertotalS4 ///
proplate1S4 verylateS4 iepS4 ///
propabsenceS5 proptruancyS5 tempexclS5cat propsicktotalS5 ///
propfamholtotalS5 propexcepdomcirctotalS5 propothertotalS5 ///
proplate1S5 verylateS5 iepS5 ///
fsmS5 seedcodeS5 lacodeS5 continueS5 ///
propabsenceS6 proptruancyS6 tempexclS6cat propsicktotalS6 ///
propfamholtotalS6 propexcepdomcirctotalS6 propothertotalS6 ///
proplate1S6 verylateS6 iepS6 ///
fsmS6 seedcodeS6 lacodeS6 continueS6




describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/recode.dta", replace

use "${pdata}/recode.dta", clear
tab continueS5 continueS6		// Check if there is anyone this sample who did not go to S5 but went to S6 * 
clear
*==============================================================================*


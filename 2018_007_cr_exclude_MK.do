/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_exclude.do
Task:         Select attendance variables for SLS members                                                     
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-01-15                                                                           
==============================================================================*/


use "${odata}/exclusion.dta", clear

sort slsno census_year stage n_exclus_1

encode stage, gen(stage2) // change the stage from a string variable to numeric
encode census_year, gen(year)

sort slsno year stage2 n_exclus_1
order slsno year stage2 n_exclus_1 n_exclus_2 noprovdays est_duration


bysort slsno (stage2): egen totexclnoprov=sum(noprovdays) if n_exclus_2==1 // overall half days lost with no alt provision due to exclusion
bysort slsno (stage2): egen totexclinc=count (n_exclus_1) // overall number of exclusions per SLS member
bysort slsno (stage2): egen totduration=sum (est_duration) if n_exclus_2==1 // overall days of exclusions per SLS member

order slsno year stage2 n_exclus_1 n_exclus_2 noprovdays est_duration totexclnoprov totexclinc totduration


bysort slsno (stage2): egen totexclnoprovS33=sum(noprovdays) if (stage2==2 & n_exclus_2==1) // total half days lost with no alt provision due to exclusion in S3
bysort slsno (stage2): egen totexclincS33=count (n_exclus_1) if stage2==2 // total number of exclusions per SLS member in S3
bysort slsno (stage2): egen totdurationS33=sum (est_duration) if (stage2==2 & n_exclus_2==1) // overall days of exclusions per SLS member in S3

order slsno year stage2 n_exclus_1 n_exclus_2 noprovdays est_duration totexclnoprov totexclnoprovS33 totexclinc totexclincS33 totduration totdurationS33

bysort slsno (stage2): egen totexclnoprovS44=sum(noprovdays) if stage2==3 & n_exclus_2==1 // total half days lost with no alt provision due to exclusion in S4
bysort slsno (stage2): egen totexclincS44=count (n_exclus_1) if stage2==3 // total number of exclusions per SLS member in S4
bysort slsno (stage2): egen totdurationS44=sum (est_duration) if stage2==3 & n_exclus_2==1 // overall days of exclusions per SLS member in S4

order slsno year stage2 n_exclus_1 n_exclus_2 noprovdays est_duration totexclnoprov totexclnoprovS33 totexclnoprovS44 totexclinc totexclincS33 totexclincS44 totduration totdurationS33 totdurationS44

bysort slsno (stage2): egen totexclnoprovS55=sum(noprovdays) if stage2==4 & n_exclus_2==1 // total half days lost with no alt provision due to exclusion in S5
bysort slsno (stage2): egen totexclincS55=count (n_exclus_1) if stage2==4 // total number of exclusions per SLS member in S5
bysort slsno (stage2): egen totdurationS55=sum (est_duration) if stage2==4 & n_exclus_2==1 // overall days of exclusions per SLS member in S5


bysort slsno (stage2): egen totexclnoprovS66=sum(noprovdays) if stage2==5 & n_exclus_2==1 // total half days lost with no alt provision due to exclusion in S6
bysort slsno (stage2): egen totexclincS66=count (n_exclus_1) if stage2==5 // total number of exclusions per SLS member in S6
bysort slsno (stage2): egen totdurationS66=sum (est_duration) if stage2==5 & n_exclus_2==1 // overall days of exclusions per SLS member in S6


bysort slsno: egen totexclnoprovS3=max(totexclnoprovS33) 
bysort slsno: egen totexclnoprovS4=max(totexclnoprovS44)
bysort slsno: egen totexclnoprovS5=max(totexclnoprovS55)
bysort slsno: egen totexclnoprovS6=max(totexclnoprovS66)

bysort slsno: egen totexclincS3=max(totexclincS33) 
bysort slsno: egen totexclincS4=max(totexclincS44) 
bysort slsno: egen totexclincS5=max(totexclincS55) 
bysort slsno: egen totexclincS6=max(totexclincS66) 

bysort slsno: egen totdurationS3=max(totdurationS33)
bysort slsno: egen totdurationS4=max(totdurationS44)
bysort slsno: egen totdurationS5=max(totdurationS55)
bysort slsno: egen totdurationS6=max(totdurationS66)


gen exclreason=incidenttype
recode exclreason 34 35 48 60/65 68 69 71/77 84=1 36 37 78/81=2  53/55=3 ///
57 58 39 46 66 67 38 99=4
lab var exclreason "Reason for exclusion"
lab def exclreason 1 "Physical violence" 2 "Verbal abuse" ///
3 "Persistent disobedience" 4 "Other"
lab val exclreason exclreason


gen physvio1=0 if stage2==2
replace physvio1=1 if exclreason==1 & stage2==2
gen physvio2=0 if stage2==3
replace physvio2=1 if exclreason==1 & stage2==3
gen physvio3=0 if stage2==4
replace physvio3=1 if exclreason==1 & stage2==4
gen physvio4=0 if stage2==5
replace physvio4=1 if exclreason==1 & stage2==5

gen verbabu1=0 if stage2==2
replace verbabu1=1 if exclreason==2 & stage2==2
gen verbabu2=0 if stage2==3
replace verbabu2=1 if exclreason==2 & stage2==3
gen verbabu3=0 if stage2==4
replace verbabu3=1 if exclreason==2 & stage2==4
gen verbabu4=0 if stage2==5
replace verbabu4=1 if exclreason==2 & stage2==5


gen persdis1=0 if stage2==2
replace persdis1=1 if exclreason==3 & stage2==2
gen persdis2=0 if stage2==3
replace persdis2=1 if exclreason==3 & stage2==3
gen persdis3=0 if stage2==4
replace persdis3=1 if exclreason==3 & stage2==4
gen persdis4=0 if stage2==5
replace persdis4=1 if exclreason==3 & stage2==5

gen oth1=0 if stage2==2
replace oth1=1 if exclreason==4 & stage2==2
gen oth2=0 if stage2==3
replace oth2=1 if exclreason==4 & stage2==3
gen oth3=0 if stage2==4
replace oth3=1 if exclreason==4 & stage2==4
gen oth4=0 if stage2==5
replace oth4=1 if exclreason==4 & stage2==5


bysort slsno: egen physvioS3=max(physvio1)
bysort slsno: egen physvioS4=max(physvio2)
bysort slsno: egen physvioS5=max(physvio3)
bysort slsno: egen physvioS6=max(physvio4)
bysort slsno: egen verbabuS3=max(verbabu1)
bysort slsno: egen verbabuS4=max(verbabu2)
bysort slsno: egen verbabuS5=max(verbabu3)
bysort slsno: egen verbabuS6=max(verbabu4)
bysort slsno: egen persdisS3=max(persdis1)
bysort slsno: egen persdisS4=max(persdis2)
bysort slsno: egen persdisS5=max(persdis3)
bysort slsno: egen persdisS6=max(persdis4)
bysort slsno: egen othS3=max(oth1)
bysort slsno: egen othS4=max(oth2)
bysort slsno: egen othS5=max(oth3)
bysort slsno: egen othS6=max(oth4)


duplicates drop slsno, force

/* recode totexclnoprovS3 -***  -*** -***=.a // missing on this variable*/
/*  *** codes removed */


*------------------------------------------------------------------------------*

*log using "${odata}/cr_exclusion.log", replace

keep slsno totexclnoprovS3 totexclnoprovS4 totexclnoprovS5 totexclnoprovS6 ///
totexclincS3 totexclincS4 totexclincS5 totexclincS6 physvioS3 physvioS4 ///
physvioS5 physvioS6 verbabuS3 verbabuS4 verbabuS5 verbabuS6 persdisS3 ///
persdisS4 persdisS5 persdisS6 othS3 othS4 othS5 othS6 ///
totdurationS3 totdurationS4 totdurationS5 totdurationS6

describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/exclude.dta", replace



*==============================================================================*

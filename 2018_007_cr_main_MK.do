/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_main.do
Task:         Select Census variables for SLS member                                                     
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-01-15                                                                           
==============================================================================*/

/*------------------------------------------------------------------------------
Notes: This is the data for the SLS members (i.e the pupils in the survey).
This data is extracted from the main census?
  
------------------------------------------------------------------------------*/

use "${odata}\slsmember.dta", clear

*------------------------------------------------------------------------------*


gen mbday = ym(dobyr,dobmt) // Create monthly bday variable
format mbday %tm

gen numbsib=dpcfam0 // Create number of siblings
recode numbsib -88 -99 1=. 3/7=1 8/13=2 14/18=3
lab var numbsib "Number of siblings"
lab def numbsib 1 "No sibling" 2 "One sibling" 3 "Two or more siblings"
lab val numbsib numbsib

gen famstruc=fmtfam0 // Create family structure variable
recode famstruc -9 -8 3 6=. 
lab val famstruc fmtfam0

gen chealth=heap0
recode chealth -9 -8 =.
lab val chealth heap0

gen tenure=tenh0 // Both LANH0 and TENH0 should be selected together due to a 
					*discrepancy between the two, namely that there are some people 
					*that are rent free in TENH0 but have a lanlord in LANH0. 
					
recode tenure -9 =.
lab val tenure tenh0

gen cars=cavh0
recode cars -99 =.
lab val cars cavh0

recode depedh0 -9=.
recode depemh0 -9=.
recode dephdh0 -9=.
recode dephsh0 -9=.
recode deptnh0 -9=.

*log using "${texts}/cr_main.log", replace

keep slsno cpres traceout mbday dobyr sex numbsib famstruc chealth tenure cars dz2001_simdscore4_0 ///
dz2001_density0 dz2001_carsco0 dz2001_eduscore4_0 dz2001_empscore4_0 ///
dz2001_healscore4_0 dz2001_housscore4_0 dz2001_incscore4_0 oa2001_urshs80_0 ///
depedh0 depemh0 dephdh0 dephsh0 deptnh0

describe                  // show all variables contained in data
notes                     // show all notes contained in data
*duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings

save "${pdata}\main.dta", replace

*==============================================================================*

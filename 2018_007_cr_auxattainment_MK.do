/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_auxattainment.do
Task:         Auxiliary datatset to create attainment variables                                                    
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-01-15                                                                          
==============================================================================*/


use "${pdata}/merge.dta", clear


*------------------------------------------------------------------------------*


keep slsno 					// SLS members who have information on SES (present in 2001 census) and absenteeism


describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/auxattainment.dta", replace



*==============================================================================*


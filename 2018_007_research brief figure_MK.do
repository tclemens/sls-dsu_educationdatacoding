/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/

coefplot (absenceS4, label(" ") nokey pstyle(p1)) (absenceformS4, label(" ") nokey pstyle(p1)), bylabel("S4")  || ///
(absenceS5S6, label(" ") nokey pstyle(p1)) (absenceformS5S6, label(" ") nokey pstyle(p1)), bylabel("S5/S6")  ||  ///
(absencefd, label(" ") nokey pstyle(p1)) (absenceformfd, label(" ") nokey pstyle(p1)), bylabel("FD")  ||  , ///
xline(0) xlabel(-0.10(0.02)0.02)  ///
keep (propabsenceS4 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4) ///
byopts(compact row(1) noiytick) xsize(7) norecycle legend(r(1)) xtitle("Standardised tariff points") /// 
coeflabels(propabsenceS4 = "Overall absences" proptruancyS4 = "Truancy" propsicktotalS4 = "Sickness absence" propfamholtotalS4 = "Family holidays" propexcepdomcirctotalS4 = "Exceptional domestic circumstances") 
graph save "$wdir\03docu\figures\absent1", replace
graph export "$wdir\03docu\figures\absent1.png", replace



coefplot (absenceS4, label(" ") nokey pstyle(p1)) (absenceformS4, label(" ") nokey pstyle(p1)), bylabel("S4")  || ///
(absenceS5S6, label(" ") nokey pstyle(p1)) (absenceformS5S6, label(" ") nokey pstyle(p1)), bylabel("S5/S6")  || , ///
xline(0) xlabel(-0.10(0.02)0.02)  ///
keep (propabsenceS4 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4) ///
byopts(compact row(1) noiytick) xsize(6) norecycle legend(r(1)) xtitle("Standardised tariff points") /// 
coeflabels(propabsenceS4 = "Overall absences" proptruancyS4 = "Truancy" propsicktotalS4 = "Sickness absence" propfamholtotalS4 = "Family holidays" propexcepdomcirctotalS4 = "Exceptional domestic circumstances") 
graph save "$wdir\03docu\figures\absent2", replace
graph export "$wdir\03docu\figures\absent2.png", replace

/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_father.do
Task:         Select Census variables for SLS members' parents                                                     
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-01-15                                                                           
==============================================================================*/


use "${odata}/parents.dta", clear

*------------------------------------------------------------------------------*

keep if sex0==1 // Restriction to fathers

duplicates report slsno
rename lsrelat0 fathrelat 

gen fatheduc=hlqp0 // Father's education
recode fatheduc -88 -99=.
lab val fatheduc hlqp0


gen fathethnic=ethp0 // Father's ethnicity
recode fathethnic -999=. 101/182=0 else=1
lab val fathethnic ethp0
lab var fathethnic "Ethnicity of Father"
label define fathethnic 0 "White" 1 "Non-white"
label value fathethnic fathethnic


gen fathill=illp0 // Father's long-term illnes
recode fathill -9=.
lab val fathill illp0


gen fathclass=socnssp0 // Father's class
recode fathclass -99=.
lab val fathclass socnssp0

gen fathhelp=help0 // Father's caring responsibility
recode fathhelp -9=.
lab val fathhelp help0


*------------------------------------------------------------------------------*

*log using "${odata}/cr_father.log", replace

keep slsno fathrelat fatheduc fathethnic fathill fathclass fathhelp

describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/father.dta", replace


*==============================================================================*

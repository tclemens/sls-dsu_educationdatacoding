/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/



/*==============================================================================
File name:    00master.do
Task:         Sets up and executes analysis                                                      
Project:      School absenteeism and attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-12-09                                                                           
==============================================================================*/


/*------------------------------------------------------------------------------ 
Content:

#1 installs ado files used in the analysis
#2 settings
#3 specifies directories in globals
#4 specifies order and task of code files and runs them
------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
Notes:

2018_007_extract_1.dta --> slsmember.dta
2018_007_extract_2.dta --> schoolcensus.dta
2018_007_extract_3.dta --> attendance.dta
2018_007_extract_4.dta --> exclusion.dta
2018_007_extract_5.dta --> attainment1.dta
2018_007_extract_6.dta --> attainment2.dta
2018_007_extract_7.dta --> parents.dta
2018_007_extract_8.dta --> grandparents.dta
datazone2001_simd2006.dta  --> simd2006.dta
datazone2001_simd2009v2 reduced --> simd2009.dta
2018_007_extract_isd_1 --> srm02
2018_007_extract_isd_2 --> 48review


------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
#1 Install ado files                                                         
------------------------------------------------------------------------------*/


*ssc install blindschemes, replace // Color scheme for plots
*ssc install coefplot, replace     // Formatting and exporting output as plot



/*------------------------------------------------------------------------------
#2 Stata settings
------------------------------------------------------------------------------*/

version 16.1          // Stata version control
clear all             // clear memory
macro drop _all       // delete all macros
set linesize 82       // result window has room for 82 chars in one line
set more off, perm    // prevents pause in results window
set scheme plottig  // sets color scheme for graphs
set matsize 1000      // size of data matrix


/*------------------------------------------------------------------------------
#3 Define globals for folder directories
------------------------------------------------------------------------------*/

global wdir "Z:\Programs\P3 Absenteeism and Attainment"

global odata "$wdir\01source\origdata"  // path to original data
global pdata "$wdir\02process\procdata" // path to processed data
global code  "$wdir\02process\code"     // path to do-files
global cbook "$wdir\03docu\codebooks"   // path to codebooks
global plots "$wdir\03docu\figures"     // path to figures
global texts "$wdir\03docu\tables"      // path to logfiles and tables


/*------------------------------------------------------------------------------
#4 Specify name, task and sequence of code files to run
------------------------------------------------------------------------------*/


do "${code}\cr_main.do"
do "${code}\cr_mother.do"
do "${code}\cr_father.do"
do "${code}\cr_grandpar.do"
do "${code}\cr_schoolcen.do"
do "${code}\cr_auxschoolcen.do"
do "${code}\cr_attend.do"
do "${code}\cr_exclude.do"
do "${code}\cr_merge.do"
do "${code}\cr_recode.do"
do "${code}\cr_auxattainment.do"
do "${code}\cr_attainment_S4.do"
do "${code}\cr_attainment_S5_S6.do"
do "${code}\cr_dropout_weight.do"
do "${code}\an_attainment_S4.do"
do "${code}\an_attainment_S5_S6.do"
do "${code}\an_robustfd.do"           




*==============================================================================*

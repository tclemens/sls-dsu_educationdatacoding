/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_dropout_weight.do
Task:         Calculate censoring weights for dropout                                                    
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-10-28                                                                          
==============================================================================*/


use  "${pdata}/attainment_S4.dta", clear
merge 1:1 slsno using "${pdata}/recode.dta"
drop if _merge==2 							// Delete cases from using that do not have SQA data
drop _merge
sort slsno
rename totalucas totalucasS4
rename maths mathsS4
rename english englishS4
rename numsubj numsubjS4
rename totlanguage totlanguageS4
rename totucaslang totucaslangS4
rename totscitech totscitechS4
rename totucasscitech totucasscitechS4


merge 1:1 slsno using "${pdata}/attainment_S5_S6.dta"
drop if _merge==2
* Censoring of students who do not have attainment data in S5/S6
gen cens=0
replace cens=1 if _merge==1 
rename totalucas totalucasS6
rename totmaths totmathsS6
rename totucasmaths totucasmathsS6
rename totenglish totenglishS6
rename totucasenglish totucasenglishS6
rename numsubj numsubjS6
rename totlanguage totlanguageS6
rename totucaslang totucaslangS6
rename totscitech totscitechS6
rename totucasscitech totucasscitechS6


replace cens=1 if continueS5==0 



/*------------------------------------------------------------------------------
#1 Estimate censoring weights
------------------------------------------------------------------------------*/

rename sex female

 
 * missing values counter
egen missnr=rowmiss(numbsib famstruc female mothage age pareth urbrur granny iepS4 ///
 chealth parill parhelp pareduc simd2001_ext fsmS4 tenure2 parclasscat ///
 cohort propabsenceS4 proptruancyS4 tempexclS4cat propsicktotalS4 ///
 propfamholtotalS4 propexcepdomcirctotalS4 totalucasS4) 


* Sample size 
gen ansample=0
replace ansample=1 if missnr==0

tab cens if ansample==1


* model for denominator
logit cens totalucasS4 propabsenceS4 proptruancyS4 tempexclS4cat propsicktotalS4 ///
propfamholtotalS4 propexcepdomcirctotalS4 /// 
i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 ///
i.numbsib i.famstruc if ansample==1, ///
vce(cluster seedcodeS4)
est store A

* prob. of censoring
predict prcensden1 if e(sample), pr


* model for numerator
logit cens if ansample==1, vce(cluster seedcodeS4) 
est store B


* prob. of censoring
predict prcensnum1 if e(sample), pr


* stabilized censoring weight
gen scw_drop=(1-prcensnum1)/(1-prcensden1) if cens==0
sum scw_drop, det
tab scw_drop



/*------------------------------------------------------------------------------
@2 Label weights
------------------------------------------------------------------------------*/

label var scw_drop     "stabilized censoring weight"


*------------------------------------------------------------------------------*

keep slsno scw_drop 

describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/scw_dropout.dta", replace


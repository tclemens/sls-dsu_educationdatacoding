/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    an_attainment_S5_S6.do
Task:         Analysis of attainment in S5/S6                                                     
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2021-09-09                                                                           
==============================================================================*/


use  "${pdata}/attainment_S4.dta", clear
merge 1:1 slsno using "${pdata}/recode.dta"
drop if _merge==2 							// Delete cases from using that do not have SQA data
drop _merge
sort slsno
rename totalucas totalucasS4
rename maths mathsS4
rename english englishS4
rename numsubj numsubjS4
rename totlanguage totlanguageS4
rename totucaslang totucaslangS4
rename totscitech totscitechS4
rename totucasscitech totucasscitechS4


merge 1:1 slsno using "${pdata}/attainment_S5_S6.dta"
drop if _merge==2 
tab _merge continueS5 // ***SDC*** cases continue in S5 according to the school census but do not have any attainment data in S5/S6
drop if _merge==1 // Remove all cases that do not have attainment data in S5/S6
drop _merge
rename totalucas totalucasS6
rename totmaths totmathsS6
rename totucasmaths totucasmathsS6
rename totenglish totenglishS6
rename totucasenglish totucasenglishS6
rename numsubj numsubjS6
rename totlanguage totlanguageS6
rename totucaslang totucaslangS6
rename totscitech totscitechS6
rename totucasscitech totucasscitechS6


keep if continueS5==1 // Restrict sample to students who continue school after S4 according to school census data



merge 1:1 slsno using "${pdata}/scw_dropout.dta"
drop if _merge==2
drop _merge



rename sex female

* missing values counter
egen missnr=rowmiss(numbsib famstruc female mothage age pareth urbrur granny iepS4 ///
 chealth parill parhelp pareduc simd2001_ext fsmS4 tenure2 parclasscat ///
 cohort propabsenceS5 proptruancyS5 tempexclS5cat propsicktotalS5 ///
 propfamholtotalS5 propexcepdomcirctotalS5 totalucasS4 totalucasS6 ///
 fsmS5 iepS5 scw_drop) 


* Sample size 
gen ansample=0
replace ansample=1 if missnr==0
keep if ansample==1





log using "${texts}/attainment_S5_S6.log", replace


*				Descriptive statistics S5***
**-------------------------------------------------------------------------------------

** Univariate statistics S5 variables

kdensity totalucasS6

* Continuous variables
estpost tabstat propabsenceS5 proptruancyS5 ///
propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 totalucasS6 [aw=scw_drop], ///
listwise stat(mean sd n) col(stat)


* Categorical variables
tab1 tempexclS5cat ///
fsmS5 iepS5 [aw=scw_drop]


** Bivariate statistics

*Correlations between exposure and outcome variables 
corr propabsenceS5 proptruancyS5 propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 totalucasS6 totalucasS4 [aw=scw_drop]


* Create percentages from proportions

foreach var of varlist propabsenceS5 proptruancyS5  propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 propabsenceS4 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4 ///
 {
replace `var'=`var'*100
}

* Standardise outcome variable
egen ztotalucasS6 = std(totalucasS6)


***			Modelling tariff score in S5/S6 ******
**-------------------------------------------------------------------------------------

**Overall absence

* Covariate model 
regress ztotalucasS6 propabsenceS5 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 i.numbsib i.famstruc ///
i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat [pw=scw_drop], vce(cluster seedcodeS5)

*Adjusted model including attainment in S4
regress ztotalucasS6 propabsenceS5 i.female mothage age i.cohort i.pareth i.urbrur ///
 i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 ///
i.numbsib i.famstruc i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat ///
totalucasS4 [pw=scw_drop], ///
vce(cluster seedcodeS5)


*Adjusted model including attainment and absence in S4
regress ztotalucasS6 propabsenceS5 i.female mothage age i.cohort i.pareth i.urbrur ///
 i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 ///
i.numbsib i.famstruc i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat ///
totalucasS4 propabsenceS4 [pw=scw_drop], ///
vce(cluster seedcodeS5)
vif
corr propabsenceS4 propabsenceS5


* Specific forms of absenteeism

* Covariate model 
regress ztotalucasS6 proptruancyS5 propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 i.numbsib i.famstruc ///
i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat [pw=scw_drop], vce(cluster seedcodeS5)

*Adjusted model including attainment in S4
regress ztotalucasS6 proptruancyS5 propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 i.numbsib i.famstruc ///
i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat totalucasS4 [pw=scw_drop], vce(cluster seedcodeS5)


* Test of differential relationships

test proptruancyS5=propsicktotalS5
test proptruancyS5=propfamholtotalS5
test proptruancyS5=propexcepdomcirctotalS5
test propsicktotalS5=propfamholtotalS5
test propsicktotalS5=propexcepdomcirctotalS5
test propfamholtotalS5=propexcepdomcirctotalS5

*Adjusted model including attainment and absence in S4
regress ztotalucasS6 proptruancyS5 propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 i.numbsib i.famstruc ///
i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat totalucasS4 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4 [pw=scw_drop], vce(cluster seedcodeS5)
vif
corr proptruancyS5 propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4


* Test of differential relationships

test proptruancyS5=propsicktotalS5
test proptruancyS5=propfamholtotalS5
test proptruancyS5=propexcepdomcirctotalS5
test propsicktotalS5=propfamholtotalS5
test propsicktotalS5=propexcepdomcirctotalS5
test propfamholtotalS5=propexcepdomcirctotalS5


* For research brief figure

drop propabsenceS4 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4

rename propabsenceS5 propabsenceS4
rename proptruancyS5 proptruancyS4
rename propsicktotalS5 propsicktotalS4
rename propfamholtotalS5 propfamholtotalS4
rename propexcepdomcirctotalS5 propexcepdomcirctotalS4

*Adjusted model including attainment in S4
regress ztotalucasS6 propabsenceS4 i.female mothage age i.cohort i.pareth i.urbrur ///
 i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 ///
i.numbsib i.famstruc i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat ///
totalucasS4 [pw=scw_drop], ///
vce(cluster seedcodeS5)
estimates store absenceS5S6

*Adjusted model including attainment in S4
regress ztotalucasS6 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4 i.female mothage age i.cohort i.pareth i.urbrur ///
i.granny i.iepS4 i.chealth i.parill i.parhelp ///
i.simd2001_ext b5.pareduc i.parclasscat i.tenure2 i.fsmS4 i.numbsib i.famstruc ///
i.iepS5 i.fsmS5 tempexclS4cat tempexclS5cat totalucasS4 [pw=scw_drop], vce(cluster seedcodeS5)
estimates store absenceformS5S6

log close




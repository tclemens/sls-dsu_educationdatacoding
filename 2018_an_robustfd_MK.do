/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/

/*==============================================================================
File name:    an_robustfd.do
Task:         Run First Difference Regression                                                     
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-12-09                                                                           
==============================================================================*/


use  "${pdata}/attainment_S4.dta", clear
merge 1:1 slsno using "${pdata}/recode.dta"
drop if _merge==2 							// Delete cases from using that do not have SQA data
drop _merge
sort slsno
rename totalucas totalucasS4
rename maths mathsS4
rename english englishS4
rename numsubj numsubjS4
rename totlanguage totlanguageS4
rename totucaslang totucaslangS4
rename totscitech totscitechS4
rename totucasscitech totucasscitechS4


merge 1:1 slsno using "${pdata}/attainment_S5_S6.dta"
drop if _merge==2 
tab _merge continueS5 // *** cases continue in S5 according to the school census but do not have any attainment data in S5/S6
drop if _merge==1 // Remove all cases that do not have attainment data in S5/S6
drop _merge
rename totalucas totalucasS6
rename totmaths totmathsS6
rename totucasmaths totucasmathsS6
rename totenglish totenglishS6
rename totucasenglish totucasenglishS6
rename numsubj numsubjS6
rename totlanguage totlanguageS6
rename totucaslang totucaslangS6
rename totscitech totscitechS6
rename totucasscitech totucasscitechS6


keep if continueS5==1 // Restrict sample to students who continue school after S4 according to school census data



merge 1:1 slsno using "${pdata}/scw_dropout.dta"
drop if _merge==2
drop _merge



rename sex female

* missing values counter
egen missnr=rowmiss(numbsib famstruc female mothage age pareth urbrur granny iepS4 ///
 chealth parill parhelp pareduc simd2001_ext fsmS4 tenure2 parclasscat ///
 cohort propabsenceS5 proptruancyS5 tempexclS5cat propsicktotalS5 ///
 propfamholtotalS5 propexcepdomcirctotalS5 totalucasS4 totalucasS6 ///
 fsmS5 iepS5 scw_drop) 


* Sample size 
gen ansample=0
replace ansample=1 if missnr==0
keep if ansample==1


* Create percentages from proportions
foreach var of varlist propabsenceS4 proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4 ///
propabsenceS5 proptruancyS5 propsicktotalS5 propfamholtotalS5 propexcepdomcirctotalS5 ///
 {
replace `var'=`var'*100
}


rename (totalucasS6 totalucasS4 propabsenceS5 propabsenceS4 proptruancyS5 ///
proptruancyS4 propsicktotalS5 propsicktotalS4 propfamholtotalS5 ///
propfamholtotalS4 propexcepdomcirctotalS5 propexcepdomcirctotalS4 ///
iepS4 iepS5 fsmS4 fsmS5) ///
(totalucas2 totalucas1 propabsence2 propabsence1 proptruancy2 proptruancy1 ///
propsicktotal2 propsicktotal1 famholtotal2 famholtotal1 ///
excepdomtotal2 excepdomtotal1 ///
iep1 iep2 fsm1 fsm2)


keep slsno totalucas2 totalucas1 propabsence2 proptruancy2  ///
propsicktotal2 propabsence1 proptruancy1 propsicktotal1 ///
famholtotal2 famholtotal1 ///
excepdomtotal2 excepdomtotal1 ///
iep1 iep2 fsm1 fsm2 



reshape long totalucas propabsence proptruancy propsicktotal famholtotal ///
excepdomtotal iep fsm, i(slsno) j(time)

xtset slsno time
sort slsno time

*** First difference estimator

gen dtotalucas = totalucas - L.totalucas          // L. is the lag-operator
gen dpropabsence = propabsence - L.propabsence   
gen dproptruancy = proptruancy - L.proptruancy
gen dpropsicktotal = propsicktotal - L.propsicktotal
gen dfamholtotal = famholtotal - L.famholtotal
gen dexcepdomtotal = excepdomtotal - L.excepdomtotal
gen dfsm = fsm - L.fsm 
gen diep = iep - L.iep
foreach var of varlist dfsm diep {
replace `var'=`var'+2
}

egen zdtotalucas = std(dtotalucas)




log using "${texts}/Robustfd.log", replace

* Overall absence
regress zdtotalucas dpropabsence, noconstant vce(cluster slsno)
regress zdtotalucas dpropabsence b2.diep b2.dfsm, noconstant vce(cluster slsno)
			
* Truancy
regress zdtotalucas dproptruancy, noconstant vce(cluster slsno)
regress zdtotalucas dproptruancy b2.diep b2.dfsm, noconstant vce(cluster slsno)

* Sickness absence
regress zdtotalucas dpropsicktotal, noconstant vce(cluster slsno)
regress zdtotalucas dpropsicktotal b2.diep b2.dfsm, noconstant vce(cluster slsno)

* Family holidays
regress zdtotalucas dfamholtotal, noconstant vce(cluster slsno)
regress zdtotalucas dfamholtotal b2.diep b2.dfsm, noconstant vce(cluster slsno)

* Exceptional domestic circumstances
regress zdtotalucas dexcepdomtotal, noconstant vce(cluster slsno)
regress zdtotalucas dexcepdomtotal b2.diep b2.dfsm, noconstant vce(cluster slsno)

* All specific forms together

regress zdtotalucas dproptruancy dpropsicktotal dfamholtotal dexcepdomtotal, noconstant vce(cluster slsno)
regress zdtotalucas dproptruancy dpropsicktotal dfamholtotal dexcepdomtotal b2.diep b2.dfsm, noconstant vce(cluster slsno)



* For research brief


rename dpropabsence propabsenceS4
rename dproptruancy proptruancyS4
rename dpropsicktotal propsicktotalS4
rename dfamholtotal propfamholtotalS4
rename dexcepdomtotal propexcepdomcirctotalS4


regress zdtotalucas propabsenceS4 b2.diep b2.dfsm, noconstant vce(cluster slsno)
estimates store absencefd
regress zdtotalucas proptruancyS4 propsicktotalS4 propfamholtotalS4 propexcepdomcirctotalS4 b2.diep b2.dfsm, noconstant vce(cluster slsno)
estimates store absenceformfd





log close









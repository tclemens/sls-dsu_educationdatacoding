/*********** *********************************************
**  Syntax file created for the SLS Project 2018_007
**          Social Inequalities in Educational Attainment:
**          An Investigation into the Mediating Role of School Absenteeism
**          https://sls.lscs.ac.uk/projects/view/2018_007/
**
** Syntax file created by the Project Team 
**          All correspondence to the PI:
**          Dr Markus Klein (University of Strathclyde)
**          markus.klein@strath.ac.uk
********** *********************************************/


/*==============================================================================
File name:    cr_mother.do
Task:         Select Census variables for SLS members' parents                                                     
Project:      School absenteeism and educational attainment
Author(s):    SD, ES, MK                                           
Last update:  2020-01-15                                                                           
==============================================================================*/

/*------------------------------------------------------------------------------
Notes:
	included NCR (Non-resident students with code -8 or -888 to the category for missing)
	
------------------------------------------------------------------------------*/


use "${odata}/parents.dta", clear




*------------------------------------------------------------------------------*

keep if sex0==2 // Restriction to mothers

duplicates report slsno
duplicates list slsno
sort slsno lsrelat0

duplicates tag slsno, gen(dup)
tab slsno lsrelat0 if dup==1 // shows slsmember has both mother and stepmother information but number is too small
duplicates drop slsno, force // drops informtion on step mother

rename lsrelat0 mothrelat 

gen motheduc=hlqp0 // mother's education
recode motheduc -88 -99=.
lab val motheduc hlqp0


gen mothethnic=ethp0 // mother's ethnicity
recode mothethnic -999=. -888=. 101/182=0 else=1
lab val mothethnic ethp0
lab var mothethnic "Ethnicity of mother"
label define mothethnic 0 "White" 1 "Non-white"
label value mothethnic mothethnic
 
gen mothill=illp0 // mother's long-term illnes
recode mothill -9=. -8=.
lab val mothill illp0


gen mothclass=socnssp0 // mother's class
recode mothclass -99=. -88=.
lab val mothclass socnssp0


gen mothhelp=help0 // mother's caring responsibility
recode mothhelp -9=. -8=.
lab val mothhelp help0

gen mothyob=dobyr0
recode mothyob -9999 1901/1907 1980/2001=.


*------------------------------------------------------------------------------*

*log using "${odata}/cr_mother.log", replace

keep sls mothrelat motheduc mothethnic mothill mothclass mothhelp mothyob

describe                  // show all variables contained in data
notes                     // show all notes contained in data
duplicates report slsno  // duplicated observations?
inspect                   // distributions, #obs , missings


save "${pdata}/mother.dta", replace



*==============================================================================*
